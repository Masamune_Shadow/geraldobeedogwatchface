#include <pebble.h>

static Window *window;
//hey let's not have shit fucking names 
BitmapLayer*	lyrWatchface;
GBitmap* 		imgBackground;
TextLayer*		txtlyrTime;

GFont fontTime; 
ResHandle resTime;	
GRect fraTimeFrame;

#define TIMEFRAME (GRect(80,90,60,60))

//time = 83 x 97
//temp = 87 x 115

static void window_load(Window *window) 
{
	
	fraTimeFrame = TIMEFRAME;
	//create the BG and attach it
	imgBackground = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND);
	lyrWatchface = bitmap_layer_create(GRect(0,0,144,168));
	bitmap_layer_set_background_color(lyrWatchface,GColorClear);
	bitmap_layer_set_compositing_mode(lyrWatchface, GCompOpSet);	
	bitmap_layer_set_bitmap(lyrWatchface, imgBackground);
	layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(lyrWatchface));	
	layer_mark_dirty(bitmap_layer_get_layer(lyrWatchface));
	
	
	
	//load the custom font
	resTime = resource_get_handle(RESOURCE_ID_FONT_QUOTED_POSITIVITY_30);
	fontTime = fonts_load_custom_font(resTime);
	//create the text layer, put it on top of the watchface layer.
	txtlyrTime = text_layer_create(fraTimeFrame);
	text_layer_set_font(txtlyrTime, fontTime);
	text_layer_set_text_color(txtlyrTime, GColorWhite);
	text_layer_set_background_color(txtlyrTime,GColorClear);
	text_layer_set_text_alignment(txtlyrTime, GTextAlignmentCenter);
	
	//duplicated so that it shows up first when we get the next.
	time_t tTime = time(NULL);
	struct tm* tmTime = localtime(&tTime);
	static char time_text[] = "22:22"; // Needs to be static because it's used by the system later.
	char *time_format;
	 if (clock_is_24h_style()) {
		time_format = "%R";
	  } else {
		time_format = "%I:%M";
	  }
	  
	 strftime(time_text, sizeof(time_text), time_format, tmTime);	 
	 text_layer_set_text(txtlyrTime, (char*)time_text);
	 layer_mark_dirty(text_layer_get_layer(txtlyrTime));
	layer_add_child(bitmap_layer_get_layer(lyrWatchface), text_layer_get_layer(txtlyrTime)); 
	
}

static void handle_minute_tick(struct tm* t , TimeUnits units_changed) 
{
	/*
	time_t tTime = time(NULL);
	struct tm* tmTime = localtime(&tTime);
	*/
	static char time_text[] = "22:22"; // Needs to be static because it's used by the system later.
	char *time_format;
	 if (clock_is_24h_style()) {
		time_format = "%R";
	  } else {
		time_format = "%I:%M";
	  }
	  
	 strftime(time_text, sizeof(time_text), time_format, t);	 
	 text_layer_set_text(txtlyrTime, (char*)time_text);
	 layer_mark_dirty(text_layer_get_layer(txtlyrTime));
	
}

static void init(void) {
  window = window_create();
  window_set_background_color(window, GColorBlack);
  window_set_window_handlers(window, (WindowHandlers) {
    .load = window_load,
    //.unload = window_unload,
  });  
  
  tick_timer_service_subscribe(MINUTE_UNIT, handle_minute_tick);
	
  window_stack_push(window, true);
}

static void deinit(void) {
	
	layer_remove_from_parent(bitmap_layer_get_layer(lyrWatchface));
	bitmap_layer_destroy(lyrWatchface);
	gbitmap_destroy(imgBackground);
	layer_remove_from_parent(text_layer_get_layer(txtlyrTime));
	text_layer_destroy(txtlyrTime);
	fonts_unload_custom_font(fontTime);	
	
	window_destroy(window);
	
}

int main(void) {
  init();
  app_event_loop();
  deinit();
}
